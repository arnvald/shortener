class Link < ActiveRecord::Base

  self.primary_key = 'uid'

  has_many :visits

  validates :uid, presence: true, uniqueness: true
  validates :url, presence: true, format: { with: /\Ahttp(s)?:\/\/.+\Z/ }

  before_validation :generate_uid, on: :create

  private
  def generate_uid
    uid = SecureRandom.hex(4)
    while Link.where(uid: uid).exists? do
      uid = SecureRandom.hex(4)
    end
    self.uid = uid
  end

end
