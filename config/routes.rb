Rails.application.routes.draw do

  get '/(:link_id)', to: 'links#show', as: :link
  post '/links', to: 'links#create', as: :create_link

end
