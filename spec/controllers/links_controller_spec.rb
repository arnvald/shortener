require 'rails_helper'

describe LinksController do

  let(:link) { Link.create url: 'http://kaligo.com' }

  describe "GET #show" do

    context 'with existing uid' do
      let(:request) { get :show, link_id: link.uid }

      before { link }

      specify { expect(request && response).to be_redirect }
      specify { expect(request && response.code).to eq '302' }

      it 'increases visit count' do
        expect { request }.to change { link.visit_count }.by 1
      end
    end

    context 'with non-existing uid' do
      before { get :show, link_id: link.uid + "aa" }

      specify { expect(response).not_to be_redirect }
      specify { expect(response.code).to eq '404' }
      specify { expect(response.body).to eq "invalid URL" }
    end

  end

  describe "POST #create" do

    context 'with correct params' do
      before { post :create, url: "http://kaligo.com", format: :json }

      specify { expect(response.body).to eq({url: "http://test.host/" + Link.last.uid}.to_json) }
      specify { expect(response.code).to eq '201' }
    end

    context 'with incorrect params' do
      before { post :create, url: "tp://kaligo.com", format: :json }

      specify { expect(response.body).to eq({errors: {url: ["is invalid"]}}.to_json) }
      specify { expect(response.code).to eq '422' }
    end

  end

end
