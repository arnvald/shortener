require 'rails_helper'

describe Link do

  it "creates an object" do
    expect { Link.create(url: "http://google.com") }.to change { Link.count }.by 1
  end

  it "does not create link with invalid url" do
    expect { Link.create(url: "p://google.com") }.not_to change { Link.count }
  end

  it "generates different uids" do
    link = Link.create(url: "http://google.com")
    link2 = Link.create(url: "http://google.com")

    expect(link.uid).not_to eq link2.uid
  end
    
end
