class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links, id: false do |t|
      t.string :uid, null: false, unique: true
      t.string :url, null: false

      t.index :uid
    end
  end
end
