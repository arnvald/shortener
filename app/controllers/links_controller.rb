class LinksController < ApplicationController

  skip_before_filter :verify_authenticity_token

  def show
    link = Link.find_by_uid(params[:link_id])

    respond_to do |format|
      format.html do
        if link.present?
          link.increment! :visit_count
          redirect_to link.url
        else
          render text: 'invalid URL', status: :not_found
        end
      end
    end
  end

  def create
    link = Link.new(params.permit(:url))

    respond_to do |format|
      format.json do
        if link.save
          render json: {url: link_url(link)}, status: :created
        else
          render json: {errors: link.errors }, status: :unprocessable_entity
        end
      end
    end
  end
  
end
